import express from "express";

export const router = express.Router();
export default { router };

router.get("/", (req, res) => {
	const params = {
		nombre: req.query.nombre,
		nivel: req.query.nivel,
		pH: req.query.pH,
		horas: req.query.horas,
		cH: req.query.cH
	};
	res.render("pago", params);
});

router.post("/", (req, res) => {
	const params = {
		nombre: req.body.nombre,
		nivel: req.body.nivel,
		pH: req.body.pH,
		horas: req.body.horas,
		cH: req.body.cH
	};
	res.render("resultados", params);
});